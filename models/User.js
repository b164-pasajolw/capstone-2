const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	fullName: {
		type: String,
		required: [true, "Full name is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile no. is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders:[
		{
			bookId: {
				type: String,
				required: [true, "Book ID is required"]
			},

			orderedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Purchased"
			}
		}

	]


})

module.exports = mongoose.model("User", userSchema)