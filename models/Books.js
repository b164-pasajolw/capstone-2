const mongoose = require("mongoose");

const bookSchema = new mongoose.Schema({
	title: {
		type: String,
		require: [true, "Title is required"]
	},

	author: {
		type: String,
		require: [true, "Author is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isAvailable: {
		type: Boolean,
		default: true
	},

	postedOn: {
		type: Date,
		default: new Date()
	},

	quantity: {
		type: Number,
		required: [true, "Quantity is required"]
	},

	buyers: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
		}
	]

})

module.exports = mongoose.model("Book", bookSchema);