const express = require ("express");
const router = express.Router();
const BookController = require("../controllers/bookControllers");

const auth = require('../auth');


//Retrieving ALL books
router.get("/all", (req, res) => {
	BookController.getAllBooks().then(result => res.send(result));
})


//Retrieving all ACTIVE books
router.get("/activeBooks", (req, res) => {
	BookController.getActiveBooks().then(result => res.send(result))
});


//retrieving a specific book
router.get("/:bookId", (req,res) => {
	//console.log(req.params.bookId);

	BookController.getAbook(req.params.bookId).then(result => res.send(result))
})



//auth for admin when creating a product / add a product
router.post("/bookPosting", auth.verify, (req, res) => {

	const data =  {
		book:req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
	BookController.addBook(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You are not authorized."})
	}
})


//updating a product information
router.put("/:bookId/updates", auth.verify, (req, res) => {
const data = {
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}
	
	if(data.isAdmin){
		BookController.newDetails(req.params.bookId, req.body).then(result => res.send(result));
	} else {
		res.send("You are not authorized.")
	}
})



//Archiving a Product
router.put('/:bookId/archive',auth.verify, (req, res) =>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		BookController.archiveBook(req.params.bookId).then(result => res.send(true))
	} else {
		res.send(false);
	}

})


module.exports = router;