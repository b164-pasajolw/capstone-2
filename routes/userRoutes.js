const express = require("express");
const router = express.Router();
const auth = require("../auth")

const UserController = require("../controllers/userControllers")

const BookController = require("../controllers/bookControllers");


//Registration for user
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})


//User Authentication (login)
router.post("/user", (req, res) => {
	UserController.userAuth(req.body).then(result => res.send(result));
});


//Set as admin
router.put('/:userId/setAdmin',auth.verify, (req, res) =>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.setAdmin(req.params.userId).then(result => res.send("User has been set as Admin."))
	} else {
		res.send(false);
	}

})



//Buy a product/book
router.post("/buy", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		bookId : req.body.bookId
	}

	UserController.buy(data).then(result => res.send(result))
	  
})


//Retrieval of Authenticated Users' Order/ Individual Order
router.get("/orders", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	UserController.getOrders(userData.id).then(result => res.send(result))
})


//Retrieval of All Order by the Admin
router.get("/checkOrder", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
		if(data.isAdmin) {
		UserController.viewOrders().then(result => res.send(result));
	} else {
		res.send(false)
	}
})


//delete/terminate a user
router.delete("/:userId/deleteUser", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
		if (data.isAdmin) {
	UserController.deleteUser(req.params.userId).then(result => res.send(result))
} else {
		res.send(false)
	}
})


//Retrieval of All Order by the Admin
router.get("/checkOrder", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
		if(data.isAdmin) {
		UserController.viewOrders().then(result => res.send(result));
	} else {
		res.send(false)
	}
})



//remove an order
router.delete("/deleteBook", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		bookId : req.body.bookId
	}

	UserController.deleteOrder(data).then(result => res.send(result))
	  
})


router.post("/checkEmail", (req, res) => {
	UserController.checkemailExists(req.body).then(result => res.send(result))
});



router.get("/details", auth.verify, (req, res) => {
	

	const userData = auth.decode(req.headers.authorization);

	UserController.getProfile(userData.id).then(result => res.send(result))
})


module.exports = router; 