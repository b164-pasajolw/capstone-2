const Book = require("../models/Books") ;



//Retrieving ALL books
module.exports.getAllBooks = (data) => {

	let exclude = {

		buyers: 0,
	}

	return Book.find(data, exclude).then( result => {

		return result
	})
}


//Retrieve all Active books
module.exports.getActiveBooks = () => {
	return Book.find({ isAvailable: true }).then(result => {
		return result;
	})
}


//retrieving a specific book
module.exports.getAbook = (reqParams) => {
	return Book.findById(reqParams).then(result => {
		return result;
	})
}


//auth for admin when creating a product / add a product
module.exports.addBook = (reqBody) => {

	//console.log(reqBody);

	let newBook = new Book({
		title: reqBody.book.title,
		author: reqBody.book.author,
		price: reqBody.book.price,
		quantity: reqBody.book.quantity
		
	})

	return newBook.save().then((course, error) => {
		if (error) {
			return false;
		} else{
			return true;
		}
	})
}


//updating a product information
module.exports.newDetails = (bookId, reqBody) => {
	

	let updates = {
		title: reqBody.title,
		author: reqBody.author,
		price: reqBody.price,
		quantity: reqBody.quantity
	};

	return Book.findByIdAndUpdate(bookId, updates).then((book, error) => {
		
		if(error) {
			return (false);
		} else {
			return true;
		}
	})
}


//Archiving a Product
module.exports.archiveBook = (reqParams) => {
	

	let toArchive = {
		isAvailable : false
	}

	return Book.findByIdAndUpdate(reqParams, toArchive).then((book, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}
