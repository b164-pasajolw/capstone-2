const User = require("../models/User");


const Books = require("../models/Books");

const bcrypt = require('bcrypt');

const auth = require("../auth");


//Register a user
module.exports.registerUser = (reqBody) => {

	
	let newUser = new User ({
		fullName: reqBody.fullName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	
return newUser.save().then((user, error) => {
	
	if(error) {
		return false;
	} else {
		
		return true
	}
})


}


//User Authentication
module.exports.userAuth = (reqBody) => {
	

	return User.findOne({ email: reqBody.email }).then(result => {
		
		if(result == null){
			return false;
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			
			if(isPasswordCorrect){
				
				return { accessToken : auth.createAccessToken(result.toObject())}
				
			} else {
				
				return false;
			}
		};
	});
};


//Set as admin
module.exports.setAdmin = (reqParams) => {
	

	let asAdmin = {
		isAdmin : true
	}

	return User.findByIdAndUpdate(reqParams, asAdmin).then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}


//Buy a product/book
module.exports.buy = async (data) => {
	

	let isUserUpdated = await User.findById(data.userId).then( user => {
			

			user.orders.push({ bookId: data.bookId});

			//save
			return user.save().then( (user, error) => {
				if (error) {
					return false;
				} else {
					return true
				}
			})
	})

	let isBookUpdated = await Books.findById(data.bookId).then(books => {
		
		books.buyers.push({ userId: data.userId });

		return books.save().then((book, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})

	})

		//Validation
		if(isUserUpdated && isBookUpdated){
			return true;
		} else {
			//user enrollment failed
			return false;
		}

}



	

	/*
	//Buy a product/book
	module.exports.buy = async (data) => {
		

		let isUserUpdated = await User.findById(data.userId).then( user => {
				
				user.hasOrdered = true;

				user.orders.push({ 
					bookId: data.bookId,
					quantity: data.quantity
				});

				//save
				return user.save().then( (user, error) => {
					if (error) {
						return false;
					} else {
						return true
					}
				})
		});

		let isBookUpdated = await Books.findById(data.bookId).then(books => {
			
			books.quantity -= data.quantity;

			books.buyers.push({ 
				userId: data.userId,
				quantityOrdered: data.quantityOrdered 
			});

			return books.save().then((book, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})

		})


	//Validation
	if(isUserUpdated && isBookUpdated){
		return ("User has purchased a book.");
	} else {
		
		return false;
	}

}
*/

//Retrieval of Authenticated Users' Order/ Individual Order
module.exports.getOrders = (data) => {


	return User.findById(data).then(result => {
		
		result.password = "";		
		return result;
	})
}


//Retrieval of All Order by the Admin
module.exports.viewOrders = () => {

	let excludedData = {
			
			mobileNo : 0,
			password: 0,
			isAdmin: 0,
			"orders._id": 0
			
	}

	return User.find({isAdmin : false}, excludedData).then(result => {
			return result

	})
}


//delete/terminate a user
module.exports.deleteUser = (userId) => {
	return User.findByIdAndRemove(userId).then((removeUser, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return ("User successfully removed!");
		}
	})
}



//remove an order
module.exports.deleteOrder = async (data) => {
	

	let isUserUpdated = await User.findById(data.userId).then( user => {
			

			user.orders.pop({ bookId: data.bookId});

			//save
			return user.save().then( (user, error) => {
				if (error) {
					return false;
				} else {
					return true
				}
			})
	})

	let isBookUpdated = await Books.findById(data.bookId).then(books => {
		
		books.buyers.pop({ userId: data.userId });

		return books.save().then((book, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})

	})


	//Validation
	if(isUserUpdated && isBookUpdated){
		return ("User has deleted an order.");
	} else {
		
		return false;
	}

}


module.exports.checkemailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		//if match is found
		if(result.length > 0){
			return true;
		} else {
			//No duplicate email found
			//User is not yet registered in the database
			return false;
		}
	})
}


module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
		result.password = "";

		return result;
	})
}